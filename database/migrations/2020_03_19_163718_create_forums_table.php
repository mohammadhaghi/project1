<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;


class CreateForumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forums', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->text('body');
            $table->integer('user_id');

            $table->timestamps();
        });

        Schema::create('replies',function (Blueprint $table){
            $table->integer('id')->autoIncrement();
            $table->integer('user_id');
            $table->integer('forum_id');

            $table->text('reply');
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forums');
    }
}
