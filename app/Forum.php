<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    public function reply()
    {
       return $this->hasMany(Reply::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
