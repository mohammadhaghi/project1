<?php

namespace App\Http\Controllers;

use App\Forum;
use App\Reply;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function main()
    {
        $forums = Forum::all();
        return view('forum.forum' , compact('forums'));
    }

    public function forum(Forum $forum)
    {
        $forum->load('reply.user' , 'user');

        return view('forum.page' , compact( 'forum'));
    }

    public function reply(Request $request , Forum $forum)
    {

        $reply = new Reply;
        $reply -> reply = $request -> reply;
        $reply -> user_id = auth()->user()->id;
        $forum -> reply()-> save($reply);

        flash('Reply added');
        return back();
    }
}
