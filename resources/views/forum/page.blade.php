@extends('layouts.app')


@section('nav-bar')
    <li class="nav-item">
        <a class="nav-link" href="/home"> Coins</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/wallet"> Wallet</a>
    </li>


@endsection

@section('content')
    <a class="btn btn-primary btn-lg" href="/forum" role="button" style="margin-left: 15% ; margin-right: 15%">Back</a>

    <div class="jumbotron" style="margin-left: 15% ; margin-right: 15% ; margin-top: 3%">
        <h1 class="display-4"> {{ $forum -> user -> name }} </h1>
        <p class="lead"> {{ $forum -> body }} </p>
        <hr class="my-4">

        @foreach($forum -> reply as $reply)
            <h3> {{ $reply -> user -> name }} </h3>
            <p> {{ $reply -> reply }} </p>
            <hr class="my-4" style="margin-right: 15%; margin-left: 15%">
        @endforeach
    </div>


    <form action="/reply/{{ $forum ->id }}" method="post" style="margin-left: 15% ; margin-right: 15% ; margin-top: 3%">
        {{csrf_field()}}
        <div class="form-group" >
            <textarea name="reply" title="reply" class="form-control"></textarea>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Reply</button>
        </div>
    </form>





@endsection
