@extends('layouts.app')


@section('nav-bar')
    <li class="nav-item">
        <a class="nav-link" href="/home"> Coins</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/wallet"> Wallet</a>
    </li>


@endsection


@section('content')
    <a class="btn btn-primary btn-lg" href="/home" role="button" style="margin-left: 15% ; margin-right: 15%">Back</a>

    <ul class="list-group" style="margin-left: 15% ; margin-right: 15% ;margin-top: 4%">

        @foreach($forums as $forum)
            <a href="/forum/{{ $forum -> id }}">
                <li class="list-group-item" style="margin-top: 1.5%"> {{ $forum -> body }} </li>
            </a>
        @endforeach


    </ul>

@endsection
